demo.state1 = function(){};

let cursors
let vel = 500

demo.state1.prototype = {
  preload: function(){
    game.load.tilemap('field', 'assets/tilemaps/field.json', null, Phaser.Tilemap.TILED_JSON)
    game.load.image('grassTiles', 'assets/tilemaps/grassTiles.png')
    game.load.image('rockTiles', 'assets/tilemaps/rockTiles.png')
    game.load.spritesheet('adam', 'assets/spritesheets/adamSheet.png', 240, 370)
  },
  create: function(){
    game.physics.startSystem(Phaser.Physics.ARCADE)
    game.stage.backgroundColor = '#dddddd';
    addChangeStateEventListeners();

    var map = game.add.tilemap('field')
    map.addTilesetImage('grassTiles')
    map.addTilesetImage('rockTiles')

    grass = map.createLayer('grass')
    rocks = map.createLayer('rocks')

    map.setCollisionBetween(1, 9, true, 'rocks')
    map.setCollision(11, true, 'grass')

    adam = game.add.sprite(centerX - 300, centerY - 200, 'adam')
    adam.anchor.setTo(0.5,0.5)
    adam.scale.setTo(0.2,0.2)
    game.physics.enable(adam)

    cursors = game.input.keyboard.createCursorKeys()
  },
  update: function(){

    game.physics.arcade.collide(adam, rocks, function(){console.log('stone hit')})
    game.physics.arcade.collide(adam, grass, function(){console.log('grass hit')})

    if(cursors.up.isDown) {
      adam.body.velocity.y = -vel
    } else if (cursors.down.isDown) {
      adam.body.velocity.y = vel
    } else {
      adam.body.velocity.y = 0
    }
    if(cursors.left.isDown) {
      adam.body.velocity.x = -vel
    } else if (cursors.right.isDown) {
      adam.body.velocity.x = vel
    } else {
      adam.body.velocity.x = 0
    }
  }
};