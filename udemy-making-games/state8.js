var text 

WebFontConfig = {
  google: {
    families: ['Revalia']
  }
};

demo.state8 = function(){};
demo.state8.prototype = {
  preload: function(){
    game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
  },
  create: function(){
    game.stage.backgroundColor = '#612f80';
    addChangeStateEventListeners();

    text = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente labore quasi quaerat quam magnam omnis ab! Enim dolorum iusto, distinctio iure et consequatur minima voluptatem, blanditiis labore quis ad at in alias corrupti quo mollitia earum vitae excepturi dolorem debitis.'

    this.spellOutText(100, 100, 1000, text, 30, 40, '#fff', 'Revalia')
  },
  update: function(){},

  spellOutText: function(x, y, width, text, fontSize, speed, fill, font) {
    var sentence = game.add.text(x, y, '', {fontSize: fontSize + 'px', fill: fill, font: font})
    var currentLine = game.add.text(10, 10, '', {fontSize: fontSize + 'px', font: font})
    currentLine.alpha = 0
    var loop = game.time.events.loop(speed, addChar)

    var index = 0

    function addChar() {
      sentence.text += text[index]
      currentLine.text += text[index]

      if (currentLine.width > width && text[index] == ' ') {
        sentence.text += '\n'
        currentLine.text = ''
      }
      if (index >= text.length - 1) {
        game.time.events.remove(loop)
        console.log('stop')
      }
      index++
    }
  }
};

